package com.example.labandroid24good.fragments

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.labandroid24good.database.AppDatabase
import com.example.labandroid24good.R
import com.example.labandroid24good.Receiver
import com.example.labandroid24good.Receiver2
import com.example.labandroid24good.database.User
import com.example.labandroid24good.database.UserAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_activity1.*
import org.jetbrains.anko.alarmManager
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


class Activity1Fragment : Fragment() {


    lateinit var alarmManager: AlarmManager

    companion object {
        fun newInstance() = Activity1Fragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view: View=inflater.inflate(R.layout.fragment_activity1, container, false)
        return view
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cal=Calendar.getInstance()
        val year= cal.get(Calendar.YEAR)
        val month =cal.get(Calendar.MONTH)
        val day= cal.get(Calendar.DAY_OF_MONTH)
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            AppDatabase::class.java,
            "users_database"
        )
            .allowMainThreadQueries()
            .build()

      TimePickButton.setOnClickListener {

          val timeSetListener = TimePickerDialog.OnTimeSetListener {timePicker, hour, minute ->
              cal.set(Calendar.HOUR_OF_DAY,hour)
              cal.set(Calendar.MINUTE, minute)
              TimeTextView.text= SimpleDateFormat("HH:mm").format(cal.time)
          }
          TimePickerDialog(activity,timeSetListener,cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),true).show()
      }

DatePickButton.setOnClickListener{
    val dpd= DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener{ view, mYear, mMonth,mDay ->
        DateTextView.setText(""+mDay+"/"+((mMonth.toInt()+1).toString())+"/"+mYear)
        cal.set(Calendar.YEAR,mYear)
        cal.set(Calendar.MONTH,mMonth)
        cal.set(Calendar.DAY_OF_MONTH,mDay)
    },year,month,day)

    dpd.show()
}

AlarmButton.setOnClickListener{
    alarmManager= context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val intent= Intent(context, Receiver2::class.java)
    val pendingIntent = PendingIntent.getBroadcast(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
    if (cal.before(Calendar.getInstance())) {
        cal.add(Calendar.DATE, 1);
    }
    alarmManager.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),pendingIntent)
    Toast.makeText(context,"Notification is set!",Toast.LENGTH_LONG).show()

}




    }
    }


