package com.example.labandroid24good

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import java.util.*

class Receiver: BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        var builder = context?.let {
            NotificationCompat.Builder(it, "notifyAlarm")
                .setSmallIcon(R.drawable.ic_icon)
                .setContentTitle("Reminder!")
                .setContentText("Your alarm manager is working!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        }

        if (builder != null) {
            with(context?.let { NotificationManagerCompat.from(it) }) {
                // notificationId is a unique int for each notification that you must define
                this?.notify(200, builder.build())
            }
        }
    }
}