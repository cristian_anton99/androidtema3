package com.example.labandroid24good;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

public class NotificationHelper extends ContextWrapper {

    public static final String chanelID = "chanelID";
    public static final String chanelName = "ChanelMain";

    private NotificationManager mManager;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public NotificationHelper(Context base) {
        super(base);
        createChanels();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createChanels() {
        NotificationChannel chanel = new NotificationChannel(chanelID, chanelName, NotificationManager.IMPORTANCE_DEFAULT);
        chanel.enableLights(true);
        chanel.enableVibration(true);
        chanel.setLightColor(R.color.colorPrimary);
        chanel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(chanel);
    }

    public NotificationManager getManager() {

        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mManager;
    }

    public NotificationCompat.Builder getChanelNotification(String title, String message){
        return new NotificationCompat.Builder(getApplicationContext(), chanelID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_icon);
    }

}