package com.example.labandroid24good;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

public class Receiver2 extends BroadcastReceiver {



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder nb = notificationHelper.getChanelNotification("Notification","Your Alarm Manager is working!");
        notificationHelper.getManager().notify(1,nb.build());
    }
}

